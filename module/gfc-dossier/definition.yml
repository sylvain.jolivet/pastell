nom: 'Dossier GFC'
type: 'Relation citoyens'
description: ''
connecteur:
    - signature
    - mailsec
    - pdf-relance
    - GED
    - 'Bordereau SEDA'
    - SAE
affiche_one: false
formulaire:
    Information:
        reference:
            name: 'Référence du dossier'
            type: text
            requis: true
            multiple: false
            commentaire: ''
            index: true
        intitule:
            name: 'Intitulé du flux'
            type: text
            requis: true
            multiple: false
            commentaire: ''
            title: true
        origine:
            name: 'Origine du flux'
            type: text
            requis: false
            multiple: false
            commentaire: ''
        objet:
            name: Objet
            type: textarea
            requis: false
            multiple: false
            commentaire: ''
        priorite:
            name: Priorité
            type: text
            requis: false
            multiple: false
            commentaire: ''
        suivi:
            name: 'Affaire suivie par'
            type: text
            requis: false
            multiple: false
            commentaire: ''
        delai:
            name: 'Délai de traitement'
            type: text
            requis: false
            multiple: false
            commentaire: ''
        nom_organisme:
            name: "Nom de l'organisme"
            type: text
            requis: false
            multiple: false
            commentaire: ''
        entree:
            name: "Date d'entrée dans la collectivité"
            type: date
            requis: false
            multiple: false
            commentaire: ''
        creation:
            name: 'Date de création du flux'
            type: date
            requis: false
            multiple: false
            commentaire: ''
        document:
            name: 'Document principal'
            type: file
            requis: true
            multiple: false
            commentaire: 'Au format PDF uniquement'
            content-type: application/pdf
        metadonnees:
            name: 'Fichier contenant les métadonnées'
            type: file
            requis: false
            multiple: false
            commentaire: ''
        pj:
            name: 'Pièces jointes'
            type: file
            requis: false
            multiple: true
            commentaire: ''
    Cheminement:
        envoi_signature:
            name: 'Envoi à la signature'
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
        envoi_mailsec:
            name: Mailsec
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
        envoi_depot:
            name: 'Transmission à la GED'
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
        envoi_sae:
            name: 'Transmission au SAE'
            type: checkbox
            onchange: cheminement-change
            default: ''
            read-only: false
    iparapheur:
        envoi_iparapheur:
            no-show: true
        iparapheur_type:
            name: 'Type iparapheur'
            read-only: true
        iparapheur_sous_type:
            name: 'Sous-type iparapheur'
            requis: true
            index: true
            type: externalData
            choice-action: iparapheur-sous-type
            link_name: 'Sélectionner un sous-type'
        json_metadata:
            name: 'Métadonnées parapheur (JSON)'
            commentaire: 'Au format JSON {"cle1":"valeur1","cle2":"valeur2",...}'
            type: file
        has_date_limite:
            name: 'Utiliser une date limite'
            type: checkbox
        date_limite:
            name: 'Date limite'
            type: date
        annotation_publique:
            name: 'Annotation publique'
            type: textarea
        annotation_privee:
            name: 'Annotation privée'
            type: textarea
    'Parapheur FAST':
        envoi_fast:
            no-show: true
        fast_parapheur_circuit:
            name: 'Circuit sur le parapheur'
            type: externalData
            choice-action: iparapheur-sous-type
            link_name: 'Liste des circuits'
        fast_parapheur_circuit_configuration:
            name: 'Configuration du circuit à la volée (au format JSON)'
            commentaire: 'Si ce fichier est déposé, il remplace le circuit choisi dans le champ "Circuit sur le parapheur"'
            type: file
        fast_parapheur_email_destinataire:
            name: 'Email du destinataire'
            commentaire: "Email de la personne à qui l’on souhaite envoyer le document après sa signature<br />\nUniquement avec le mode \"circuit à la volée\""
        fast_parapheur_email_cc:
            name: 'Email des destinataires en copie carbone'
            commentaire: "Permet de rajouter des destinataires mais en copie carbone<br />\nUniquement avec le mode \"circuit à la volée\""
        fast_parapheur_agents:
            name: 'Emails des agents'
            commentaire: "Les emails des utilisateurs à rajouter en tant qu’agent. Séparé par des virgules<br />\nUniquement avec le mode \"circuit à la volée\""
    Signature:
        iparapheur_dossier_id:
            name: '#ID dossier parapheur'
            read-only: true
        iparapheur_historique:
            name: 'Historique iparapheur'
            type: file
            read-only: true
        parapheur_last_message:
            name: 'Dernier message reçu du parapheur'
            read-only: true
        parapheur_date_signature:
            name: 'Date de dernière signature'
            type: date
            read-only: true
        has_signature:
            no-show: true
            read-only: true
        signature:
            name: 'Signature détachée'
            type: file
            read-only: true
        bordereau_signature:
            name: 'Bordereau de signature'
            type: file
            read-only: true
        document_original:
            name: 'Document original'
            type: file
            read-only: true
        multi_document_original:
            name: 'Multi-document(s) original'
            type: file
            multiple: true
            read-only: true
        iparapheur_annexe_sortie:
            name: 'Annexe(s) de sortie du parapheur'
            type: file
            multiple: true
            read-only: true
    'Mail sécurisé':
        to:
            name: Destinataire(s)
            commentaire: 'Plusieurs emails possibles séparés par une virgule'
            type: mail-list
            autocomplete: MailSec/getContactAjax
        cc:
            name: 'Copie à'
            commentaire: 'Plusieurs emails possibles séparés par une virgule'
            type: mail-list
            autocomplete: MailSec/getContactAjax
        bcc:
            name: 'Copie cachée à'
            commentaire: 'Plusieurs emails possibles séparés par une virgule'
            type: mail-list
            autocomplete: MailSec/getContactAjax
        password:
            name: 'Mot de passe'
            type: password
            may_be_null: true
        password2:
            name: 'Mot de passe (confirmation)'
            type: password
            may_be_null: true
            is_equal: password
            is_equal_error: 'Les mots de passe ne correspondent pas'
        key:
            no-show: true
            read-only: true
        sent_mail_number:
            name: Destinataires
            default: '0'
            no-show: true
            read-only: true
        sent_mail_read:
            name: 'Mails lus'
            default: '0'
            no-show: true
            read-only: true
        sent_mail_answered:
            name: 'Mails répondus'
            default: '0'
            no-show: true
            read-only: true
    'Accusé de lecture':
        lecture_mail:
            no-show: true
            read-only: true
        accuse_lecture:
            name: 'Accusé de lecture'
            commentaire: 'Format pdf'
            type: file
            read-only: true
    'Retour GED':
        has_ged_document_id:
            no-show: true
            read-only: true
        ged_document_id_file:
            name: 'Identifiants des documents sur la GED'
            type: file
            visionneuse: Pastell\Viewer\GedIdDocumentsViewer
            read-only: true
            requis: true
            visionneuse-no-link: true
    'Configuration SAE':
        sae_config:
            name: 'Configuration du versement SAE'
            commentaire: "Fichier JSON {cle1:valeur1,cle2:valeur2,...}.\nLes métadonnées de ce fichier prévalent sur les métadonnées Pastell"
            type: file
    SAE:
        sae_show:
            no-show: true
        journal:
            name: 'Faisceau de preuves'
            type: file
        date_journal_debut:
            name: 'Date du premier évènement journalisé'
        date_cloture_journal:
            name: 'Date de clôture du journal'
        date_cloture_journal_iso8601:
            name: 'Date de clôture du journal (iso 8601)'
        sae_transfert_id:
            name: 'Identifiant de transfert'
            index: true
        sae_bordereau:
            name: 'Bordereau SEDA'
            type: file
        sae_archive:
            name: "Paquet d'archive (SIP)"
            type: file
        ar_sae:
            type: file
            name: 'Accusé de réception SAE'
        sae_ack_comment:
            name: "Commentaire de l'accusé de réception"
        reply_sae:
            type: file
            name: 'Réponse du SAE'
        sae_archival_identifier:
            name: "Identifiant de l'archive sur le SAE"
        sae_atr_comment:
            name: 'Commentaire de la réponse du SAE'
champs-affiches:
    - titre
    - dernier_etat
    - date_dernier_etat
    - reference
champs-recherche-avancee:
    - type
    - id_e
    - lastetat
    - last_state_begin
    - etatTransit
    - state_begin
    - notEtatTransit
    - search
    - reference
page-condition:
    iparapheur:
        envoi_iparapheur: true
    'Parapheur FAST':
        envoi_fast: true
    Signature:
        has_signature: true
    'Mail sécurisé':
        envoi_mailsec: true
    'Accusé de lecture':
        lecture_mail: true
    'Retour GED':
        has_ged_document_id: true
    'Configuration SAE':
        envoi_sae: true
    SAE:
        sae_show: true
action:
    creation:
        name-action: Créer
        name: Créé
        rule:
            no-last-action: ''
    modification:
        name-action: Modifier
        name: 'En cours de rédaction'
        rule:
            last-action:
                - creation
                - modification
                - importation
                - recu-iparapheur
                - reception
                - non-recu
                - send-ged
                - send-signature-error
                - send-mailsec-error
    supression:
        name-action: Supprimer
        name: Supprimé
        rule:
            last-action:
                - creation
                - modification
                - importation
                - termine
                - fatal-error
                - rejet-iparapheur
                - send-signature-error
                - erreur-verif-iparapheur
                - send-mailsec-error
                - error-ged
                - rejet-sae
        action-class: Supprimer
        warning: 'Êtes-vous sûr ?'
    importation:
        name: 'Importation du document'
        rule:
            role_id_e: no-role
        action-automatique: orientation
    orientation:
        name: 'Envoi du document'
        name-action: 'Envoyer le document'
        rule:
            last-action:
                - creation
                - modification
                - importation
                - recu-iparapheur
                - reception
                - non-recu
                - send-ged
                - accepter-sae
            document_is_valide: true
        action-class: OrientationTypeDossierPersonnalise
    termine:
        name: 'Traitement terminé'
        rule:
            role_id_e: no-role
    reouverture:
        name: 'Rouvrir le dossier'
        rule:
            last-action:
                - termine
        action-class: Reopen
    cheminement-change:
        no-workflow: true
        rule:
            role_id_e: 'no-role,'
        action-class: CheminementChangeTypeDossierPersonnalise
    preparation-send-iparapheur:
        name: "Préparation de l'envoi au parapheur"
        rule:
            role_id_e: no-role
        action-automatique: send-iparapheur
    send-iparapheur:
        name-action: 'Transmettre au parapheur'
        name: 'Transmis au parapheur'
        rule:
            last-action:
                - preparation-send-iparapheur
                - send-signature-error
        action-class: StandardAction
        connecteur-type: signature
        connecteur-type-action: SignatureEnvoie
        connecteur-type-mapping:
            iparapheur_type: iparapheur_type
            iparapheur_sous_type: iparapheur_sous_type
            iparapheur_dossier_id: iparapheur_dossier_id
            json_metadata: json_metadata
            fast_parapheur_circuit: fast_parapheur_circuit
            fast_parapheur_circuit_configuration: fast_parapheur_circuit_configuration
            fast_parapheur_email_destinataire: fast_parapheur_email_destinataire
            fast_parapheur_email_cc: fast_parapheur_email_cc
            fast_parapheur_agents: fast_parapheur_agents
            send-signature-error: send-signature-error
            iparapheur_annotation_publique: annotation_publique
            iparapheur_annotation_privee: annotation_privee
            iparapheur_has_date_limite: has_date_limite
            iparapheur_date_limite: date_limite
            objet: intitule
            document: document
            autre_document_attache: pj
        action-automatique: verif-iparapheur
    send-signature-error:
        name: "Erreur lors de l'envoi du dossier à la signature"
        editable-content:
            - iparapheur_sous_type
            - json_metadata
            - has_date_limite
            - date_limite
            - fast_parapheur_circuit
            - fast_parapheur_circuit_configuration
            - fast_parapheur_email_destinataire
            - fast_parapheur_email_cc
            - fast_parapheur_agents
        rule:
            role_id_e: no-role
        modification-no-change-etat: true
    verif-iparapheur:
        name-action: 'Vérifier le statut de signature'
        name: 'Vérification de la signature'
        rule:
            last-action:
                - erreur-verif-iparapheur
                - send-iparapheur
        action-class: StandardAction
        connecteur-type: signature
        connecteur-type-action: SignatureRecuperation
        connecteur-type-mapping:
            iparapheur_historique: iparapheur_historique
            parapheur_last_message: parapheur_last_message
            parapheur_date_signature: parapheur_date_signature
            has_signature: has_signature
            signature: signature
            document_original: document_original
            multi_document_original: multi_document_original
            bordereau: bordereau_signature
            iparapheur_annexe_sortie: iparapheur_annexe_sortie
            iparapheur_dossier_id: iparapheur_dossier_id
            recu-iparapheur: recu-iparapheur
            rejet-iparapheur: rejet-iparapheur
            erreur-verif-iparapheur: erreur-verif-iparapheur
            iparapheur_has_date_limite: has_date_limite
            iparapheur_date_limite: date_limite
            objet: intitule
            document: document
            autre_document_attache: pj
    erreur-verif-iparapheur:
        name: 'Erreur lors de la vérification du statut de signature'
        rule:
            role_id_e: no-role
    recu-iparapheur:
        name: 'Signature récupérée'
        rule:
            role_id_e: no-role
        editable-content:
            - envoi_mailsec
            - envoi_depot
            - envoi_sae
            - to
            - cc
            - bcc
            - password
            - password2
            - sae_config
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    rejet-iparapheur:
        name: 'Signature refusée'
        rule:
            role_id_e: no-role
    iparapheur-sous-type:
        name: 'Liste des sous-type iparapheur'
        no-workflow: true
        rule:
            role_id_e: no-role
        action-class: IparapheurSousType
        connecteur-type-mapping:
            iparapheur_type: iparapheur_type
            iparapheur_sous_type: iparapheur_sous_type
            fast_parapheur_circuit: fast_parapheur_circuit
    preparation-send-mailsec:
        name: "Préparation de l'envoi par mail sécurisé"
        rule:
            role_id_e: no-role
        action-automatique: send-mailsec
    send-mailsec:
        name-action: 'Envoyer le mail sécurisé'
        name: "Envoi d'un mail sécurisé"
        rule:
            last-action:
                - preparation-send-mailsec
                - send-mailsec-error
        action-class: StandardAction
        connecteur-type: mailsec
        connecteur-type-action: MailsecEnvoyer
        connecteur-type-mapping:
            to: to
            cc: cc
            bcc: bcc
            sent_mail_number: sent_mail_number
            send-mailsec-error: send-mailsec-error
        action-automatique: mailsec-relance
    send-mailsec-error:
        name: "Erreur d'envoi d'un mail sécurisé"
        editable-content:
            - to
            - cc
            - bcc
        rule:
            role_id_e: no-role
        modification-no-change-etat: true
    mailsec-relance:
        name: Relancé
        name-action: 'Relancer si nécessaire'
        rule:
            last-action:
                - send-mailsec
                - renvoi
                - reception-partielle
        action-class: StandardAction
        connecteur-type: pdf-relance
        connecteur-type-action: MailsecRelance
        connecteur-type-mapping:
            reception-partielle: reception-partielle
            prepare-renvoi: prepare-renvoi
            non-recu: preparation-non-recu
            send-mailsec: send-mailsec
        action-automatique: mailsec-relance
    prepare-renvoi:
        name: 'Préparation de la relance du mail'
        rule:
            role_id_e: no-role
        action-automatique: renvoi
    renvoi:
        name-action: 'Envoyer à nouveau'
        name: Renvoyé
        rule:
            last-action:
                - mailsec-relance
                - send-mailsec
                - renvoi
                - reception-partielle
            role_id_e: editeur
            document_is_valide: true
        connecteur-type: mailsec
        connecteur-type-action: MailsecRenvoyer
        action-automatique: mailsec-relance
        action-class: StandardAction
    reception:
        name: Reçu
        rule:
            role_id_e: no-role
        editable-content:
            - envoi_depot
            - envoi_sae
            - sae_config
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    reception-partielle:
        name: 'Reçu partiellement'
        rule:
            role_id_e: no-role
        action-automatique: mailsec-relance
    preparation-non-recu:
        name: 'Préparation au passage comme non reçu'
        rule:
            role_id_e: no-role
        action-automatique: non-recu
    non-recu:
        name: 'Non reçu'
        name-action: 'Définir comme non reçu'
        warning: ok
        rule:
            last-action:
                - preparation-non-recu
                - renvoi
                - send-mailsec
                - reception-partielle
        action-class: StandardAction
        connecteur-type: mailsec
        connecteur-type-action: MailsecNotReceived
        editable-content:
            - envoi_depot
            - envoi_sae
            - sae_config
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    reponse:
        name: 'Une réponse a été enregistrée'
        rule:
            no-action:
                - creation
    compute_read_mail:
        name: 'Calcul des mails lus'
        rule:
            role_id_e: no-role
        action-class: StandardAction
        connecteur-type: mailsec
        connecteur-type-action: MailsecComputeReadMail
        connecteur-type-mapping:
            sent_mail_read: sent_mail_read
    compute_answered_mail:
        name: 'Calcul des mails répondus'
        rule:
            role_id_e: no-role
        action-class: StandardAction
        connecteur-type: mailsec
        connecteur-type-action: MailsecComputeAnsweredMail
        connecteur-type-mapping:
            sent_mail_answered: sent_mail_answered
    preparation-send-ged:
        name: "Préparation de l'envoi à la GED"
        rule:
            role_id_e: no-role
        action-automatique: send-ged
    send-ged:
        name-action: 'Verser à la GED'
        name: 'Versé à la GED'
        rule:
            last-action:
                - preparation-send-ged
                - error-ged
        action-class: StandardAction
        connecteur-type: GED
        connecteur-type-action: GEDEnvoyer
        connecteur-type-mapping:
            fatal-error: error-ged
        editable-content:
            - envoi_sae
            - sae_config
            - sae_show
            - journal
            - date_journal_debut
            - date_cloture_journal
            - date_cloture_journal_iso8601
            - sae_transfert_id
            - sae_bordereau
            - sae_archive
            - ar_sae
            - sae_ack_comment
            - reply_sae
            - sae_archival_identifier
            - sae_atr_comment
        modification-no-change-etat: true
    error-ged:
        name: 'Erreur irrécupérable lors du dépôt'
        rule:
            role_id_e: no-role
    preparation-send-sae:
        name: "Préparation de l'envoi au SAE"
        rule:
            role_id_e: no-role
        action-automatique: generate-sip
    generate-sip:
        name-action: "Générer le paquet d'archive (SIP)"
        name: "Paquet d'archive (SIP) généré"
        rule:
            last-action:
                - preparation-send-sae
                - generate-sip-error
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAEGenerateArchiveAction
        action-automatique: send-archive
        connecteur-type-mapping:
            generate-sip-error: generate-sip-error
            sae_show: sae_show
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            sae_config: sae_config
            journal: journal
            date_journal_debut: date_journal_debut
            date_cloture_journal: date_cloture_journal
            date_cloture_journal_iso8601: date_cloture_journal_iso8601
    generate-sip-error:
        name: "Erreur lors de la génération de l'archive"
        rule:
            role_id_e: no-role
    send-archive:
        name-action: 'Verser au SAE'
        name: 'Versé au SAE'
        rule:
            last-action:
                - generate-sip
                - rejet-sae
                - erreur-envoie-sae
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: Pastell\Step\SAE\Action\SAESendArchiveAction
        action-automatique: verif-sae
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            sae_bordereau: sae_bordereau
            sae_archive: sae_archive
            erreur-envoie-sae: erreur-envoie-sae
    erreur-envoie-sae:
        name: "Erreur lors de l'envoi au SAE"
        rule:
            role_id_e: no-role
    verif-sae:
        name-action: "Récupérer l'AR du document sur le SAE"
        name: "Récuperation de l'AR sur le SAE"
        rule:
            last-action:
                - send-archive
                - verif-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEVerifier
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            ar_sae: ar_sae
            ar-recu-sae: ar-recu-sae
            verif-sae-erreur: verif-sae-erreur
            sae_ack_comment: sae_ack_comment
            sae_bordereau: sae_bordereau
            ack-not-provided: ack-not-provided
    verif-sae-erreur:
        name: "Erreur lors de la récupération de l'AR"
        rule:
            role_id_e: no-role
    ar-recu-sae:
        name: 'AR SAE reçu'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    ack-not-provided:
        name: 'En attente de la validation par le SAE'
        rule:
            role_id_e: no-role
        action-automatique: validation-sae
    validation-sae:
        name-action: "Vérifier l'acceptation par le SAE"
        name: "Vérification de l'acceptation par le SAE"
        rule:
            last-action:
                - ar-recu-sae
                - ack-not-provided
                - validation-sae-erreur
        action-class: StandardAction
        connecteur-type: SAE
        connecteur-type-action: SAEValider
        connecteur-type-mapping:
            sae_transfert_id: sae_transfert_id
            reply_sae: reply_sae
            erreur-envoie-sae: erreur-envoie-sae
            validation-sae-erreur: validation-sae-erreur
            accepter-sae: accepter-sae
            rejet-sae: rejet-sae
            sae_atr_comment: sae_atr_comment
            sae_archival_identifier: sae_archival_identifier
            sae_bordereau: sae_bordereau
    validation-sae-erreur:
        name: 'Erreur lors de la vérification de la validité du transfert'
        rule:
            role_id_e: no-role
    accepter-sae:
        name: 'Transfert accepté par le SAE'
        rule:
            role_id_e: no-role
    rejet-sae:
        name: 'Transfert rejeté par le SAE'
        rule:
            role_id_e: no-role
studio_definition: ewogICJpZF90eXBlX2Rvc3NpZXIiOiAiZG9zc2llci13Z2ZjIiwKICAicGFzdGVsbC12ZXJzaW9uIjogIiVWRVJTSU9OJSIsCiAgInRpbWVzdGFtcCI6IDE2MzgxNzI5NDQsCiAgInJhd19kYXRhIjogewogICAgImlkX3R5cGVfZG9zc2llciI6ICJkb3NzaWVyLXdnZmMiLAogICAgIm5vbSI6ICJEb3NzaWVyIEdGQyIsCiAgICAidHlwZSI6ICJSZWxhdGlvbiBjaXRveWVucyIsCiAgICAiZGVzY3JpcHRpb24iOiAiIiwKICAgICJub21fb25nbGV0IjogIkluZm9ybWF0aW9uIiwKICAgICJyZXN0cmljdGlvbl9wYWNrIjogIiIsCiAgICAiZm9ybXVsYWlyZUVsZW1lbnQiOiBbCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJyZWZlcmVuY2UiLAogICAgICAgICJuYW1lIjogIlJcdTAwZTlmXHUwMGU5cmVuY2UgZHUgZG9zc2llciIsCiAgICAgICAgInR5cGUiOiAidGV4dCIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIiIsCiAgICAgICAgInJlcXVpcyI6ICJvbiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICJvbiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICJvbiIsCiAgICAgICAgInRpdHJlIjogIiIsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogIiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogImludGl0dWxlIiwKICAgICAgICAibmFtZSI6ICJJbnRpdHVsXHUwMGU5IGR1IGZsdXgiLAogICAgICAgICJ0eXBlIjogInRleHQiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAib24iLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAiIiwKICAgICAgICAiY2hhbXBzX3JlY2hlcmNoZV9hdmFuY2VlIjogIiIsCiAgICAgICAgInRpdHJlIjogIm9uIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAib3JpZ2luZSIsCiAgICAgICAgIm5hbWUiOiAiT3JpZ2luZSBkdSBmbHV4IiwKICAgICAgICAidHlwZSI6ICJ0ZXh0IiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAiIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAib2JqZXQiLAogICAgICAgICJuYW1lIjogIk9iamV0IiwKICAgICAgICAidHlwZSI6ICJ0ZXh0YXJlYSIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIiIsCiAgICAgICAgInJlcXVpcyI6ICIiLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAiIiwKICAgICAgICAiY2hhbXBzX3JlY2hlcmNoZV9hdmFuY2VlIjogIiIsCiAgICAgICAgInRpdHJlIjogIiIsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogIiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogInByaW9yaXRlIiwKICAgICAgICAibmFtZSI6ICJQcmlvcml0XHUwMGU5IiwKICAgICAgICAidHlwZSI6ICJ0ZXh0IiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAiIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAic3VpdmkiLAogICAgICAgICJuYW1lIjogIkFmZmFpcmUgc3VpdmllIHBhciIsCiAgICAgICAgInR5cGUiOiAidGV4dCIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIiIsCiAgICAgICAgInJlcXVpcyI6ICIiLAogICAgICAgICJjaGFtcHNfYWZmaWNoZXMiOiAiIiwKICAgICAgICAiY2hhbXBzX3JlY2hlcmNoZV9hdmFuY2VlIjogIiIsCiAgICAgICAgInRpdHJlIjogIiIsCiAgICAgICAgInNlbGVjdF92YWx1ZSI6ICIiLAogICAgICAgICJwcmVnX21hdGNoIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2hfZXJyb3IiOiAiIiwKICAgICAgICAiY29udGVudF90eXBlIjogIiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogImRlbGFpIiwKICAgICAgICAibmFtZSI6ICJEXHUwMGU5bGFpIGRlIHRyYWl0ZW1lbnQiLAogICAgICAgICJ0eXBlIjogInRleHQiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAiIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJub21fb3JnYW5pc21lIiwKICAgICAgICAibmFtZSI6ICJOb20gZGUgbCdvcmdhbmlzbWUiLAogICAgICAgICJ0eXBlIjogInRleHQiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAiIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAiZWxlbWVudF9pZCI6ICJlbnRyZWUiLAogICAgICAgICJuYW1lIjogIkRhdGUgZCdlbnRyXHUwMGU5ZSBkYW5zIGxhIGNvbGxlY3Rpdml0XHUwMGU5IiwKICAgICAgICAidHlwZSI6ICJkYXRlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAiIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAiY3JlYXRpb24iLAogICAgICAgICJuYW1lIjogIkRhdGUgZGUgY3JcdTAwZTlhdGlvbiBkdSBmbHV4IiwKICAgICAgICAidHlwZSI6ICJkYXRlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAiIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiIgogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAiZG9jdW1lbnQiLAogICAgICAgICJuYW1lIjogIkRvY3VtZW50IHByaW5jaXBhbCIsCiAgICAgICAgInR5cGUiOiAiZmlsZSIsCiAgICAgICAgImNvbW1lbnRhaXJlIjogIkF1IGZvcm1hdCBQREYgdW5pcXVlbWVudCIsCiAgICAgICAgInJlcXVpcyI6ICJvbiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiAiIiwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiAiYXBwbGljYXRpb25cL3BkZiIKICAgICAgfSwKICAgICAgewogICAgICAgICJlbGVtZW50X2lkIjogIm1ldGFkb25uZWVzIiwKICAgICAgICAibmFtZSI6ICJGaWNoaWVyIGNvbnRlbmFudCBsZXMgbVx1MDBlOXRhZG9ublx1MDBlOWVzIiwKICAgICAgICAidHlwZSI6ICJmaWxlIiwKICAgICAgICAiY29tbWVudGFpcmUiOiAiIiwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImNoYW1wc19hZmZpY2hlcyI6ICIiLAogICAgICAgICJjaGFtcHNfcmVjaGVyY2hlX2F2YW5jZWUiOiAiIiwKICAgICAgICAidGl0cmUiOiBmYWxzZSwKICAgICAgICAic2VsZWN0X3ZhbHVlIjogIiIsCiAgICAgICAgInByZWdfbWF0Y2giOiAiIiwKICAgICAgICAicHJlZ19tYXRjaF9lcnJvciI6ICIiLAogICAgICAgICJjb250ZW50X3R5cGUiOiBmYWxzZQogICAgICB9LAogICAgICB7CiAgICAgICAgImVsZW1lbnRfaWQiOiAicGoiLAogICAgICAgICJuYW1lIjogIlBpXHUwMGU4Y2VzIGpvaW50ZXMiLAogICAgICAgICJ0eXBlIjogIm11bHRpX2ZpbGUiLAogICAgICAgICJjb21tZW50YWlyZSI6ICIiLAogICAgICAgICJyZXF1aXMiOiAiIiwKICAgICAgICAiY2hhbXBzX2FmZmljaGVzIjogIiIsCiAgICAgICAgImNoYW1wc19yZWNoZXJjaGVfYXZhbmNlZSI6ICIiLAogICAgICAgICJ0aXRyZSI6ICIiLAogICAgICAgICJzZWxlY3RfdmFsdWUiOiAiIiwKICAgICAgICAicHJlZ19tYXRjaCI6ICIiLAogICAgICAgICJwcmVnX21hdGNoX2Vycm9yIjogIiIsCiAgICAgICAgImNvbnRlbnRfdHlwZSI6ICIiCiAgICAgIH0KICAgIF0sCiAgICAiZXRhcGUiOiBbCiAgICAgIHsKICAgICAgICAibnVtX2V0YXBlIjogMCwKICAgICAgICAidHlwZSI6ICJzaWduYXR1cmUiLAogICAgICAgICJsYWJlbCI6ICJFbnZvaSBcdTAwZTAgbGEgc2lnbmF0dXJlIiwKICAgICAgICAiZGVmYXVsdENoZWNrZWQiOiBmYWxzZSwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImF1dG9tYXRpcXVlIjogIiIsCiAgICAgICAgInNwZWNpZmljX3R5cGVfaW5mbyI6IHsKICAgICAgICAgICJsaWJlbGxlX3BhcmFwaGV1ciI6ICJpbnRpdHVsZSIsCiAgICAgICAgICAiZG9jdW1lbnRfYV9zaWduZXIiOiAiZG9jdW1lbnQiLAogICAgICAgICAgImFubmV4ZSI6ICJwaiIsCiAgICAgICAgICAiaGFzX2RhdGVfbGltaXRlIjogIm9uIiwKICAgICAgICAgICJoYXNfbWV0YWRhdGFfaW5fanNvbiI6ICJvbiIsCiAgICAgICAgICAiY29udGludWVfYWZ0ZXJfcmVmdXNhbCI6ICIiCiAgICAgICAgfSwKICAgICAgICAibnVtX2V0YXBlX3NhbWVfdHlwZSI6IDAsCiAgICAgICAgImV0YXBlX3dpdGhfc2FtZV90eXBlX2V4aXN0cyI6IGZhbHNlCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAibnVtX2V0YXBlIjogMSwKICAgICAgICAidHlwZSI6ICJtYWlsc2VjIiwKICAgICAgICAibGFiZWwiOiAiTWFpbHNlYyIsCiAgICAgICAgImRlZmF1bHRDaGVja2VkIjogZmFsc2UsCiAgICAgICAgInJlcXVpcyI6ICIiLAogICAgICAgICJhdXRvbWF0aXF1ZSI6ICIiLAogICAgICAgICJzcGVjaWZpY190eXBlX2luZm8iOiBbXSwKICAgICAgICAibnVtX2V0YXBlX3NhbWVfdHlwZSI6IDAsCiAgICAgICAgImV0YXBlX3dpdGhfc2FtZV90eXBlX2V4aXN0cyI6IGZhbHNlCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAibnVtX2V0YXBlIjogMiwKICAgICAgICAidHlwZSI6ICJkZXBvdCIsCiAgICAgICAgImxhYmVsIjogIlRyYW5zbWlzc2lvbiBcdTAwZTAgbGEgR0VEIiwKICAgICAgICAiZGVmYXVsdENoZWNrZWQiOiBmYWxzZSwKICAgICAgICAicmVxdWlzIjogIiIsCiAgICAgICAgImF1dG9tYXRpcXVlIjogIiIsCiAgICAgICAgInNwZWNpZmljX3R5cGVfaW5mbyI6IFtdLAogICAgICAgICJudW1fZXRhcGVfc2FtZV90eXBlIjogMCwKICAgICAgICAiZXRhcGVfd2l0aF9zYW1lX3R5cGVfZXhpc3RzIjogZmFsc2UKICAgICAgfSwKICAgICAgewogICAgICAgICJudW1fZXRhcGUiOiAiMyIsCiAgICAgICAgInR5cGUiOiAic2FlIiwKICAgICAgICAibGFiZWwiOiAiVHJhbnNtaXNzaW9uIGF1IFNBRSIsCiAgICAgICAgImRlZmF1bHRDaGVja2VkIjogZmFsc2UsCiAgICAgICAgInJlcXVpcyI6ICIiLAogICAgICAgICJhdXRvbWF0aXF1ZSI6ICIiLAogICAgICAgICJzcGVjaWZpY190eXBlX2luZm8iOiB7CiAgICAgICAgICAic2FlX2hhc19tZXRhZGF0YV9pbl9qc29uIjogIm9uIgogICAgICAgIH0sCiAgICAgICAgIm51bV9ldGFwZV9zYW1lX3R5cGUiOiAwLAogICAgICAgICJldGFwZV93aXRoX3NhbWVfdHlwZV9leGlzdHMiOiBmYWxzZQogICAgICB9CiAgICBdCiAgfQp9
