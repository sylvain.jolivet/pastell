<?php

namespace Pastell\Client\IparapheurV5\Model;

enum Type: string
{
    case intellectualEntity = 'intellectualEntity';
    case file = 'file';
}
