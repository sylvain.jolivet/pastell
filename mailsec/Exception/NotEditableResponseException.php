<?php

declare(strict_types=1);

namespace Mailsec\Exception;

use Exception;

final class NotEditableResponseException extends Exception
{
}
