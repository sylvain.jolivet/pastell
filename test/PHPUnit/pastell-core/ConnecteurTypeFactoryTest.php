<?php

use PHPUnit\Framework\TestCase;

class ConnecteurTypeFactoryTest extends TestCase
{
    /** @var  ConnecteurTypeFactory */
    private $connecteurTypeFactory;

    protected function setUp(): void
    {
        $extensions = $this->createMock("Extensions");
        $extensions
            ->method("getAllConnecteurType")
            ->willReturn(["signature" => __DIR__ . "/fixtures/"]);

        $objectInstancier = new ObjectInstancier();
        $objectInstancier->setInstance(Extensions::class, $extensions);
        $this->connecteurTypeFactory = new ConnecteurTypeFactory($objectInstancier);
    }

    /**
     * @throws RecoverableException
     */
    public function testGetActionExecutor()
    {
        $this->assertTrue($this->connecteurTypeFactory->getActionExecutor("signature", "SignatureEnvoieMock")->go());
    }

    /**
     * @throws RecoverableException
     */
    public function testConnecteurTypeNotFound()
    {
        $this->expectException("RecoverableException");
        $this->expectExceptionMessage("Impossible de trouver le connecteur type sae");
        $this->connecteurTypeFactory->getActionExecutor("sae", "SignatureEnvoieMock")->go();
    }

    /**
     * @throws RecoverableException
     */
    public function testClassNotFound(): void
    {
        $this->expectException(RecoverableException::class);
        $this->expectExceptionMessage("La classe NotFoundMock n'a pas été trouvée.");
        $this->connecteurTypeFactory->getActionExecutor("signature", "NotFoundMock")->go();
    }

    public function testGetAllActionExecutor()
    {
        $this->assertEquals(["SignatureEnvoieMock"], $this->connecteurTypeFactory->getAllActionExecutor());
    }
}
