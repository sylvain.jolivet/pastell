<?php

class DepotFTPTest extends PastellTestCase
{
    /** @var  DepotFTP */
    private $depotFTP;

    protected function setUp(): void
    {
        parent::setUp();

        /*
         * FIXME Il y a un deprecated dans nicolab/php-ftp-client qui n'est pour le moment pas pris en compte
         * https://github.com/Nicolab/php-ftp-client/pull/67
         */
        @ $FTPClientWrapper = $this->createMock(FtpClientWrapper::class);

        $FTPClientWrapper->method('login')->willReturn(true);
        $FTPClientWrapper->method('mkdir')->willReturn(true);
        $FTPClientWrapper->method('nlist')->willReturn(['foo']);

        $connecteurConfig = $this->getDonneesFormulaireFactory()->getNonPersistingDonneesFormulaire();
        $connecteurConfig->setData(DepotFTP::DEPOT_FTP_DIRECTORY, '/foo/');

        $this->depotFTP = new DepotFTP();
        $this->depotFTP->setFtpClient($FTPClientWrapper);
        $this->depotFTP->setConnecteurConfig($connecteurConfig);
    }

    public function testList()
    {
        $this->assertEquals(
            ['foo'],
            $this->depotFTP->listDirectory()
        );
    }

    public function testMakeDirectory()
    {
        $this->assertEquals(
            '/foo/bar',
            $this->depotFTP->makeDirectory('bar')
        );
    }

    public function testSaveDocument()
    {
        $this->assertEquals(
            '/foo/foo/bar',
            $this->depotFTP->saveDocument('foo', 'bar', __DIR__ . "/fixtures/toto.txt")
        );
    }

    public function testDirectoryExists()
    {
        $this->assertFalse(
            $this->depotFTP->directoryExists('bar')
        );
    }

    public function testFileExists()
    {
        $this->assertFalse(
            $this->depotFTP->fileExists('bar')
        );
    }
}
