## Description

Ex : Le fichier envoyé au tdt doit être le document signé
Si possible mettre un imprime écran. (cliquer sur attach a file)

## Url(s) de l'erreur :

- [Pastell](https://pastell.recette.libriciel.net/:path)

## Liste des actions menant au bug

Lister ici les actions permettant de reproduire le bug

## Message d'erreur :

Lister ici le(s) message(s) d'erreur(s) afficher à l'utilisateur ou des logs  

## Ticket

- Ville [Ticket](lien vers le ticket)



/label ~"Cible:Patch"  ~"Origine: Ticket client" ~"Priorité:Haute" 
/cc @epommateau @iruiz @mreyrolle @rbarlet   
