[[_TOC_]]

# Demande de fonctionnalité
Suggérer une idée pour Pastell. Si cela ne semble pas correct, choisissez un autre template.

**Problème à résoudre**

(Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...])

**Solution proposée**

(Une description claire et concise de ce que vous voulez qu'il se passe.)

**Décrivez les alternatives que vous avez envisagées**

(Une description claire et concise de toutes les solutions ou fonctionnalités alternatives que vous avez envisagées.)

**Contexte supplémentaire**

(Ajoutez tout autre contexte ou capture d'écran sur la demande de fonctionnalité ici.)


## Tickets OTRS

- Ville [Ticket](lien vers le ticket)

/label ~"Cible:Version" ~"Origine: Ticket client" ~"Priorité::Basse"
/cc @epommateau @iruiz @mreyrolle @rbarlet   
