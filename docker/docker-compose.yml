name: pastell

services:
    web:
        image: 'registry.libriciel.fr:443/public/plateforme/pastell:canary'
        init: true
        restart: unless-stopped
        read_only: true
        stop_grace_period: 60s
        stop_signal: SIGTERM
        depends_on:
            - db
            - redis
        tmpfs:
            - /data/config:mode=770,uid=${UID:-33},gid=${GID:-33}
            - /data/run:mode=770,uid=${UID:-33},gid=${GID:-33}
            - /data/lock:mode=770,uid=${UID:-33},gid=${GID:-33}
            - /data/html_purifier:mode=770,uid=${UID:-33},gid=${GID:-33}
            - /var/run:mode=770,uid=${UID:-33},gid=${GID:-33}
            - /var/www/pastell/var:mode=770,uid=${UID:-33},gid=${GID:-33}
        volumes:
            - ${PASTELL_EXTENSION_PATH:-../../}:/data/extensions/
            - ${WORKSPACE_VOLUME:-app_workspace}:/data/workspace
            - ${PASTELL_SSL_CERTIFICAT:-app_certificate}:/data/certificate
            - ${PASTELL_SESSION:-app_session}:/var/lib/php/session
            - ${LETSENCRYPT_DATADIR:-letsencrypt_datadir}:/etc/letsencrypt
            - ${LOGS_PATH:-logs_datadir}:/data/log
            - ${TMP_PATH:-tmp_datadir}:/tmp
            - ${UPLOAD_CHUNK_PATH:-upload_chunk_datadir}:/data/upload_chunk
            - ${CACERTS_PATH:-cacerts}:/etc/ssl/certs/

        environment:
            PHP_MEMORY_LIMIT: ${PHP_MEMORY_LIMIT:-1000M}
            PHP_UPLOAD_MAX_FILESIZE: ${PHP_UPLOAD_MAX_FILESIZE:-200M}
            PHP_POST_MAX_SIZE: ${PHP_POST_MAX_SIZE:-200M}
            PHP_MAX_EXECUTION_TIME: ${PHP_MAX_EXECUTION_TIME:-600}
            REDIS_SERVER: ${REDIS_HOST:-redis}
            REDIS_PORT: ${REDIS_PORT:-6379}
            PLATEFORME_MAIL: ${PLATEFORME_MAIL:-noreply@paste.ll}
            PASTELL_SITE_BASE: ${PASTELL_SITE_BASE:-https://localhost:8443}
            WEBSEC_BASE: ${WEBSEC_BASE:-https://127.0.0.1:8443/}
            PASTELL_ADMIN_LOGIN: ${PASTELL_ADMIN_LOGIN:-admin}
            PASTELL_ADMIN_EMAIL: ${PASTELL_ADMIN_EMAIL:-test@libriciel.net}
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD:-123456}
            MYSQL_USER: ${MYSQL_USER:-user}
            MYSQL_PASSWORD: ${MYSQL_PASSWORD:-user}
            MYSQL_DATABASE: ${MYSQL_DATABASE:-pastell}
            MYSQL_HOST: db
            MYSQL_HOST_TEST: ${MYSQL_HOST_TEST:-db_test}
            LETSENCRYPT_EMAIL: ${LETSENCRYPT_EMAIL:-}
            LETSENCRYPT_DOMAIN: ${LETSENCRYPT_DOMAIN:-}
            LETSENCRYPT_MAILSEC_DOMAIN: ${LETSENCRYPT_MAILSEC_DOMAIN:-}
            SENTRY_DSN: ${SENTRY_DSN:-}
            SENTRY_ENVIRONMENT: ${SENTRY_ENVIRONMENT:-dev}
            MAILER_DSN: ${MAILER_DSN:-null://null}
            ADMIN_EMAIL: ${ADMIN_EMAIL:-test@libriciel.net}
            PACK_CHORUS_PRO: ${PACK_CHORUS_PRO:-false}
            PACK_MARCHE: ${PACK_MARCHE:-false}
            PACK_URBANISME: ${PACK_URBANISME:-false}
            PACK_RH: ${PACK_RH:-false}
            PACK_TEST: ${PACK_TEST:-false}
            PACK_LIBERSIGN: ${PACK_LIBERSIGN:-false}
            PACK_RECUP_PARAPHEUR_CORBEILLE_CONNECTOR: ${PACK_RECUP_PARAPHEUR_CORBEILLE_CONNECTOR:-false}
            PACK_RECUP_FIN_PARAPHEUR: ${PACK_RECUP_FIN_PARAPHEUR:-false}
            TIMEZONE: ${TIMEZONE:-Europe/Paris}
            NB_WORKERS: ${NB_WORKERS:-5}
            JOURNAL_MAX_AGE_IN_MONTHS: ${JOURNAL_MAX_AGE_IN_MONTHS:-2}
            UNLOK_JOB_ERROR_AT_STARTUP: ${UNLOK_JOB_ERROR_AT_STARTUP:-false}
            NB_JOB_PAR_VERROU: ${NB_JOB_PAR_VERROU:-1}
            HTTP_PROXY_URL: ${HTTP_PROXY_URL:-}
            NO_PROXY: ${NO_PROXY:-localhost,127.0.0.1,::1,seda-generator,pes-viewer}
            TOGGLE_CDGFeature: ${TOGGLE_CDGFeature:-false}
            TOGGLE_CertificateAuthentication: ${TOGGLE_CertificateAuthentication:-false}
            OPENSSL_CIPHER_STRING_SECURITY_LEVEL: ${OPENSSL_CIPHER_STRING_SECURITY_LEVEL:-2}
            USE_UUID_FOR_DOCUMENT: ${USE_UUID_FOR_DOCUMENT:-false}
            PASSWORD_MIN_ENTROPY: ${PASSWORD_MIN_ENTROPY:-80}
        ports:
            - "${WEB_HTTP_PORT:-8000}:80"
            - "${WEB_HTTPS_PORT:-8443}:443"
        user: "${UID:-33}:${GID:-33}"

    db:
        image: 'hubdocker.libriciel.fr/mariadb:10.11.3-jammy'
        restart: unless-stopped
        command:
            - "--character-set-server=utf8mb4"
            - "--collation-server=utf8mb4_unicode_ci"
        environment:
          MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD:-123456}
          MYSQL_USER: ${MYSQL_USER:-user}
          MYSQL_PASSWORD: ${MYSQL_PASSWORD:-user}
          MYSQL_DATABASE: ${MYSQL_DATABASE:-pastell}
        volumes:
            - ${MARIADB_DATADIR:-mariadb_datadir}:/var/lib/mysql/

    redis:
        image: 'hubdocker.libriciel.fr/redis:7.0.11'
        restart: unless-stopped

    seda-generator:
        image: 'registry.libriciel.fr:443/public/plateforme/seda-generator:1.0.2'
        restart: unless-stopped
        volumes:
            - '/etc/localtime:/etc/localtime:ro'

    flow:
        image: registry.libriciel.fr/public/libriciel/flow:1.0.0-rc.4
        restart: unless-stopped
    cloudooo:
        image: 'registry.libriciel.fr/public/actes/cloudooo:7.1.0'
        restart: unless-stopped

    pes-viewer:
        image: 'registry.libriciel.fr/public/signature/pes-viewer:2.0.5'
        restart: unless-stopped
        deploy:
            resources:
                limits:
                    memory: 1536M
        volumes:
            - ${PES_PJ_PATH:-/data/pesPJ}:/pesPJ/

    cacerts:
        image: registry.libriciel.fr:443/public/libriciel/ls-docker-cacerts:0.1.1
        volumes:
            - ${CACERTS_PATH:-cacerts}:/etc/ssl/certs/
            - ${LOCAL_CACERTS_PATH:-local_cacerts}:/usr/local/share/ca-certificates

volumes:
    mariadb_datadir:
    app_workspace:
    app_certificate:
    app_session:
    letsencrypt_datadir:
    logs_datadir:
    tmp_datadir:
    upload_chunk_datadir:
    cacerts:
    local_cacerts:
