<?php

namespace PHPSTORM_META {

    override(
        \ObjectInstancier::getInstance(0),
        map([
            '' => '@',
        ])
    );

    override(
        \Controler::getInstance(),
        map([
            '' => '@',
        ])
    );
}