<?php

class Fichier
{
    /** @var string */
    public $filepath;

    /** @var string */
    public $filename;

    /** @var string $content */
    public $content;

    /** @var string $contentType */
    public $contentType;
}
